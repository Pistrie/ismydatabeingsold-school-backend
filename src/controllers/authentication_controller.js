const assert = require("assert");
const jwt = require("jsonwebtoken");
const database = require("../config/database");
const logger = require("../config/config").logger;
const jwtSecretKey = require("../config/config").jwtSecretKey;

module.exports = {
  validateLogin(req, res, next) {
    logger.info("validateLogin called");
    // Verify that we receive the expected input
    try {
      assert(typeof req.body.emailAddress === "string", "email is missing!");
      assert.match(
        req.body.emailAddress,
        /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/,
        "invalid email address"
      );
      assert(typeof req.body.password === "string", "password is missing!");
      logger.info("Userdata is valid");
      next();
    } catch (err) {
      logger.info("Userdata is invalid:", err.message);
      next({
        message: err.message,
        errCode: 400,
      });
    }
  },

  validateRegister(req, res, next) {
    logger.info("validateRegister called");
    try {
      assert(typeof req.body.firstName === "string", "first name is missing!");
      assert(typeof req.body.lastName === "string", "last name is missing!");
      assert(typeof req.body.emailAddress === "string", "email is missing!");
      assert.match(
        req.body.emailAddress,
        /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/,
        "invalid email address"
      );
      assert(typeof req.body.password === "string", "password is missing!");
      logger.info("userdata is valid");
      next();
    } catch (err) {
      logger.debug("validateRegister error:", err.message);
      next({
        message: err.message,
        errCode: 400,
      });
    }
  },

  validateToken(req, res, next) {
    logger.info("validateToken called");
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorization header missing!");
      next({
        message: "Authorization header missing!",
        errCode: 401,
      });
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length);

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn("Not authorized");
          next({
            message: "Not authorized",
            errCode: 401,
          });
        } else if (payload) {
          logger.debug("token is valid", payload);
          // User heeft toegang. Voeg UserId uit payload toe aan
          // request, voor ieder volgend endpoint
          req.userId = payload.id;
          next();
        }
      });
    }
  },

  login(req, res, next) {
    logger.info("authentication-controller.login called");
    database.query(
      "SELECT `id`, `firstName`, `lastName`, `emailAddress`, `dateOfBirth`, `role`, `password` FROM `Users` WHERE `emailAddress` = ?",
      [req.body.emailAddress],
      (err, rows) => {
        // 1. kijken of het useraccount bestaat
        if (err) {
          logger.info("Error:", err.message);
          next({
            message: err.message,
            errCode: 500,
          });
        } else if (rows) {
          // 2. er was een resultaat, check het password
          logger.info("result from database:");
          logger.info(rows);
          if (rows.length === 1 && rows[0].password === req.body.password) {
            logger.info("passwords MATCH, sending valid token");
            // Create an object containing the data we want in the payload
            const payload = {
              id: rows[0].id,
            };
            // userinfo returned to the caller
            const userinfo = {
              id: rows[0].id,
              firstName: rows[0].firstName,
              lastName: rows[0].lastName,
              emailAddress: rows[0].emailAddress,
              dateOfBirth: rows[0].dateOfBirth,
              role: rows[0].role,
              token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
            };
            logger.debug("Logged in, sending:", userinfo);
            res.status(200).json(userinfo);
          } else {
            logger.info("User not found or password invalid");
            next({
              message: "User not found or password invalid",
              errCode: 400,
            });
          }
        }
      }
    );
  },

  register(req, res, next) {
    logger.info("register called");
    logger.info("register");
    logger.info(req.body);

    const { firstName, lastName, emailAddress, dateOfBirth, role, password } =
      req.body;
    database.query(
      "INSERT INTO `Users` (firstName, lastName, emailAddress, dateOfBirth, role, password) VALUES (?, ?, ?, ?, ?, ?)",
      [firstName, lastName, emailAddress, dateOfBirth, role, password],
      (err, rows, fields) => {
        if (err) {
          // if there is an error we assume the user already exists
          logger.error("Error:", err.message);
          let message;
          if (err.message.match("ER_DUP_ENTRY")) {
            message = "This email address is already in use";
          } else {
            message = err.message;
          }
          next({
            message: message,
            errCode: 400,
          });
        } else if (rows) {
          logger.trace(rows);
          // Create an object containing the data we want in the payload
          // This time we add the id of the newly inserted user
          const payload = {
            id: rows.insertId,
          };
          // Userinfo returned to the caller
          const userinfo = {
            id: rows.insertId,
            firstName: firstName,
            lastName: lastName,
            emailAddress: emailAddress,
            dateOfBirth: dateOfBirth,
            role: role,
            token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
          };
          logger.debug("Registered", userinfo);
          res.status(200).json(userinfo);
        }
      }
    );
  },

  renewToken(req, res, next) {
    logger.debug("renewToken");

    database.query(
      "SELECT * FROM `Users` WHERE `id` = ?",
      [req.userId],
      (err, rows) => {
        if (err) {
          logger.error("Error:", err.message);
          next({
            message: err.message,
            errCode: 500,
          });
        } else if (rows) {
          // User gevonden, return user info met nieuw token.
          // Create an object containing the data we want in the payload
          const payload = {
            id: rows[0].id,
          };
          // Userinfo returned to the caller
          const userinfo = {
            id: rows[0].id,
            firstName: rows[0].firstName,
            lastName: rows[0].lastName,
            emailAddress: rows[0].emailAddress,
            dateOfBirth: rows[0].dateOfBirth,
            role: rows[0].role,
            token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
          };
          logger.debug("Sending:", userinfo);
          res.status(200).json(userinfo);
        }
      }
    );
  },
};
