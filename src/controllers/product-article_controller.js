const logger = require("tracer").console();
const database = require("../config/database");
const productArticle_dao = require("../dao/product-article_dao");

module.exports = {
  create: (req, res, next) => {
    logger.log("productArticle_controller.create called");

    const { productId, articleType, description } = req.body;
    const userId = req.userId;
    const values = {
      userId: userId,
      productId: productId,
      dateOfWriting: new Date().toISOString().slice(0, 10),
      articleType: articleType,
      description: description,
    };

    productArticle_dao.create(values, (err, result) => {
      if (err) {
        next({
          message: err,
          errCode: 400,
        });
      } else {
        res.status(200).json(result);
      }
    });
  },

  getAll: (req, res, next) => {
    logger.log("productArticle_controller.getAll called");

    database.query("select * from ProductArticles", (err, result) => {
      if (err) {
        logger.log("error getting productArticles");
        next({
          message: err.message,
          errCode: 500,
        });
      } else {
        res.status(200).json(result);
      }
    });
  },

  getById: (req, res, next) => {
    logger.log("productArticle_controller.getById called");

    const productArticleId = req.params.productArticleId;

    database.query(
      "select * from ProductArticles where id = ?",
      productArticleId,
      (err, result) => {
        if (result === 0) {
          logger.log("productArticle does not exist");
          next({
            message: "productArticle does not exist",
            errCode: 404,
          });
        } else if (err) {
          logger.log("error getting productArticle by id");
          next({
            message: err.message,
            errCode: 500,
          });
        } else {
          res.status(200).json(result);
        }
      }
    );
  },

  update: (req, res, next) => {
    logger.log("productArticle_controller.update called");

    const productArticleId = req.params.productArticleId;
    const { productId, articleType, description } = req.body;
    const dateOfWriting = new Date().toISOString().slice(0, 10);
    const userId = req.userId;

    let productArticleFromStartingQuery = {};
    database.query(
      "select * from ProductArticles where id = ?",
      productArticleId,
      (err, result) => {
        if (result) {
          if (result === "") {
            next({
              message: "article not found",
              errCode: 404
            });
          } else {
            Object.keys(result).forEach(function(key) {
              productArticleFromStartingQuery = result[key];
            });
            if (productArticleFromStartingQuery.userId !== req.userId) {
              next({
                message: "You are not the creator of this product",
                errCode: 403,
              });
            } else {
              database.query(
                "update ProductArticles set productId=?, dateOfWriting=?, articleType=?, description=? where id=?",
                [productId, dateOfWriting, articleType, description, productArticleId],
                (err, result) => {
                  if (err) {
                    logger.log("error updating article", productArticleId);
                    next({
                      message: err.message,
                      errCode: 500
                    });
                  } else if (result.affectedRows === 0) {
                    logger.log("article does not exist");
                    next({
                      message: "article does not exist",
                      errCode: 404
                    });
                  } else {
                    let productArticle = {};
                    productArticle.userId = userId;
                    productArticle.productId = productId;
                    productArticle.dateOfWriting = dateOfWriting;
                    productArticle.articleType = articleType;
                    productArticle.description = description;
                    res.status(200).json(productArticle);
                  }
                }
              );
            }
          }
        }
      }
    );
  },

  delete: (req, res, next) => {
    logger.log("productArticle_controller.delete called");

    const productArticleId = req.params.productArticleId;

    let productArticleFromStartingQuery = {};
    database.query(
      "select * from ProductArticles where id = ?",
      productArticleId,
      (err, result) => {
        logger.log(result);
        if (result === 0) {
          next({
            message: "productArticle not found",
            errCode: 404,
          });
        } else {
          Object.keys(result).forEach(function(key) {
            productArticleFromStartingQuery = result[key];
          });
          if (productArticleFromStartingQuery.userId !== req.userId) {
            next({
              message:
                "You are not the creator of this article, it is " + req.userId,
              errCode: 401,
            });
          } else {
            database.query(
              "delete from ProductArticles where id = ?",
              productArticleId,
              (err, result) => {
                if (err) {
                  logger.log("error deleting article", productArticleId);
                  next({
                    message: err.message,
                    errCode: 500,
                  });
                } else if (result.affectedRows === 0) {
                  next({
                    message: "article does not exist",
                    errCode: 404,
                  });
                } else {
                  res.status(200).json(productArticleFromStartingQuery);
                }
              }
            );
          }
        }
      }
    );
  },
};
