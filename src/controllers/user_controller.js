const logger = require("tracer").console();
const database = require("../config/database");
const user_dao = require("../dao/user_dao");

module.exports = {
  create: (req, res, next) => {
    logger.log("user_controller.create called");

    const { firstName, lastName, emailAddress, dateOfBirth, role, password } =
      req.body;
    const values = {
      firstName: firstName,
      lastName: lastName,
      emailAddress: emailAddress,
      dateOfBirth: dateOfBirth,
      role: role,
      password: password,
    };

    user_dao.create(values, (err, result) => {
      if (err) {
        next({
          message: err,
          errCode: 400,
        });
      } else {
        res.status(200).json(result);
      }
    });
  },

  getAll: (req, res, next) => {
    logger.log("user_controller.getAll called");

    database.query("select * from Users", (err, result) => {
      if (err) {
        logger.log("error getting users");
        next({
          message: err.message,
          errCode: 500,
        });
      } else {
        res.status(200).json(result);
      }
    });
  },

  getById: (req, res, next) => {
    logger.log("user_controller.getById called");

    const userId = req.params.userId;

    database.query(
      "select * from Users where id = ?",
      userId,
      (err, result) => {
        if (result === 0) {
          logger.log("user does not exist");
          next({
            message: "user does not exist",
            errCode: 404,
          });
        } else if (err) {
          logger.log("error getting user by id");
          next({
            message: err.message,
            errCode: 500,
          });
        } else {
          res.status(200).json(result);
        }
      }
    );
  },

  update: (req, res, next) => {
    logger.log("user_controller.update called");

    const userId = req.params.userId;
    const { firstName, lastName, emailAddress, dateOfBirth, role, password } =
      req.body;

    let userFromStartingQuery = {};
    database.query(
      "select * from Users where id = ?",
      userId,
      (err, result) => {
        if (result) {
          if (result === "") {
            next({
              message: "user not found",
              errCode: 404,
            });
          } else {
            Object.keys(result).forEach(function (key) {
              userFromStartingQuery = result[key];
            });
            if (userFromStartingQuery.id !== req.userId) {
              next({
                message: "You are not the creator of this user",
                errCode: 403,
              });
            } else {
              database.query(
                "update Users set firstName=?, lastName=?, emailAddress=?, dateOfBirth=?, role=?, password=? where id=?",
                [
                  firstName,
                  lastName,
                  emailAddress,
                  dateOfBirth,
                  role,
                  password,
                  userId,
                ],
                (err, result) => {
                  if (err) {
                    logger.log("error updating user", userId);
                    next({
                      message: err.message,
                      errCode: 500,
                    });
                  } else if (result.affectedRows === 0) {
                    logger.log("user does not exist");
                    next({
                      message: "user does not exist",
                      errCode: 404,
                    });
                  } else if (result) {
                    let user = {};
                    user.userId = userId;
                    user.firstName = firstName;
                    user.lastName = lastName;
                    user.emailAddress = emailAddress;
                    user.dateOfBirth = dateOfBirth;
                    user.role = role;
                    user.password = password;
                    res.status(200).json(user);
                  }
                }
              );
            }
          }
        }
      }
    );
  },

  delete: (req, res, next) => {
    logger.log("user_controller.delete called");

    const userId = req.params.userId;

    let userFromStartingQuery = {};
    database.query(
      "select * from Users where id = ?",
      userId,
      (err, result) => {
        logger.log(result);
        if (result === 0) {
          next({
            message: "user not found",
            errCode: 404,
          });
        } else {
          Object.keys(result).forEach(function (key) {
            userFromStartingQuery = result[key];
          });
          if (userFromStartingQuery.id !== req.userId) {
            next({
              message: "You are not the creator of this user",
              errCode: 401,
            });
          } else {
            database.query(
              "delete from Users where id = ?",
              userId,
              (err, result) => {
                if (err) {
                  logger.log("error deleting user", userId);
                  next({
                    message: err.message,
                    errCode: 500,
                  });
                } else if (result.affectedRows === 0) {
                  logger.log("user does not exist");
                  next({
                    message: "user does not exist",
                    errCode: 404,
                  });
                } else {
                  res.status(200).json(userFromStartingQuery);
                }
              }
            );
          }
        }
      }
    );
  },
};
