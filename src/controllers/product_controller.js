const logger = require("tracer").console();
const database = require("../config/database");
const product_dao = require("../dao/product_dao");

module.exports = {
  create: (req, res, next) => {
    logger.log("product_controller.create called");

    const { companyId, name, website, releaseDate, licenseType } = req.body;
    const userId = req.userId;
    const values = {
      userId: userId,
      companyId: companyId,
      name: name,
      website: website,
      releaseDate: releaseDate,
      licenseType: licenseType,
    };

    product_dao.create(values, (err, result) => {
      if (err) {
        next({
          message: err,
          errCode: 400,
        });
      } else {
        res.status(200).json(result);
      }
    });
  },

  getAll: (req, res, next) => {
    logger.log("product_controller.getAll called");

    database.query("select * from Products", (err, result) => {
      if (err) {
        logger.log("error getting products");
        next({
          message: err.message,
          errCode: 500,
        });
      } else {
        res.status(200).json(result);
      }
    });
  },

  getById: (req, res, next) => {
    logger.log("product_controller.getById called");

    const productId = req.params.productId;

    database.query(
      "select * from Products where id = ?",
      productId,
      (err, result) => {
        if (result === 0) {
          logger.log("product does not exist");
          next({
            message: "product does not exist",
            errCode: 404,
          });
        } else if (err) {
          logger.log("error getting product by id");
          next({
            message: err.message,
            errCode: 500,
          });
        } else {
          res.status(200).json(result);
        }
      }
    );
  },

  update: (req, res, next) => {
    logger.log("product_controller.update called");

    const productId = req.params.productId;
    const { companyId, name, website, releaseDate, licenseType } = req.body;
    const userId = req.userId;

    let productFromStartingQuery = {};
    database.query(
      "select * from Products where id = ?",
      productId,
      (err, result) => {
        if (result) {
          if (result === "") {
            next({
              message: "product not found",
              errCode: 404,
            });
          } else {
            Object.keys(result).forEach(function (key) {
              productFromStartingQuery = result[key];
            });
            if (productFromStartingQuery.userId !== req.userId) {
              next({
                message: "You are not the creator of this product",
                errCode: 403,
              });
            } else {
              database.query(
                "update Products set companyId=?, name=?, website=?, releaseDate=?, licenseType=? where id=?",
                [companyId, name, website, releaseDate, licenseType, productId],
                (err, result) => {
                  if (err) {
                    logger.log("error updating product", productId);
                    next({
                      message: err.message,
                      errCode: 500,
                    });
                  } else if (result.affectedRows === 0) {
                    logger.log("product does not exist");
                    next({
                      message: "product does not exist",
                      errCode: 404,
                    });
                  } else if (result) {
                    let product = {};
                    product.userId = userId;
                    product.companyId = companyId;
                    product.name = name;
                    product.website = website;
                    product.releaseDate = releaseDate;
                    product.licenseType = licenseType;
                    res.status(200).json(product);
                  }
                }
              );
            }
          }
        }
      }
    );
  },

  delete: (req, res, next) => {
    logger.log("product_controller.delete called");

    const productId = req.params.productId;

    let productFromStartingQuery = {};
    database.query(
      "select * from Products where id = ?",
      productId,
      (err, result) => {
        logger.log(result);
        if (result === 0) {
          next({
            message: "product not found",
            errCode: 404,
          });
        } else {
          Object.keys(result).forEach(function (key) {
            productFromStartingQuery = result[key];
          });
          if (productFromStartingQuery.userId !== req.userId) {
            next({
              message: "You are not the creator of this product",
              errCode: 401,
            });
          } else {
            database.query(
              "delete from Products where id = ?",
              productId,
              (err, result) => {
                if (err) {
                  logger.log("error deleting product", productId);
                  next({
                    message: err.message,
                    errCode: 500,
                  });
                } else if (result.affectedRows === 0) {
                  logger.log("product does not exist");
                  next({
                    message: "product does not exist",
                    errCode: 404,
                  });
                } else {
                  res.status(200).json(productFromStartingQuery);
                }
              }
            );
          }
        }
      }
    );
  },
};
