const logger = require("tracer").console();
const database = require("../config/database");
const company_dao = require("../dao/company_dao");

module.exports = {
  create: (req, res, next) => {
    logger.log("company_controller.create called");

    const { name, website, foundedDate, numberOfEmployees } = req.body;
    const userId = req.userId;
    const values = {
      userId: userId,
      name: name,
      website: website,
      foundedDate: foundedDate,
      numberOfEmployees: numberOfEmployees,
    };

    company_dao.create(values, (err, result) => {
      if (err) {
        next({
          message: err,
          errCode: 400,
        });
      } else {
        res.status(200).json(result);
      }
    });
  },

  getAll: (req, res, next) => {
    logger.log("company_controller.getAll called");

    database.query("select * from Companies", (err, result) => {
      if (err) {
        logger.log("error getting companies");
        next({
          message: err.message,
          errCode: 500,
        });
      } else {
        res.status(200).json(result);
      }
    });
  },

  getById: (req, res, next) => {
    logger.log("company_controller.getById called");

    const companyId = req.params.companyId;

    database.query(
      "select * from Companies where id = ?",
      companyId,
      (err, result) => {
        if (result === 0) {
          logger.log("company does not exist");
          next({
            message: "company does not exist",
            errCode: 404,
          });
        } else if (err) {
          logger.log("error getting company by id");
          next({
            message: err.message,
            errCode: 500,
          });
        } else {
          res.status(200).json(result);
        }
      }
    );
  },

  update: (req, res, next) => {
    logger.log("company_controller.update called");

    const companyId = req.params.companyId;
    const { name, website, foundedDate, numberOfEmployees } = req.body;
    const userId = req.userId;

    let companyFromStartingQuery = {};
    database.query(
      "select * from Companies where id = ?",
      companyId,
      (err, result) => {
        if (result) {
          if (result === "") {
            next({
              message: "company not found",
              errCode: 404,
            });
          } else {
            Object.keys(result).forEach(function (key) {
              companyFromStartingQuery = result[key];
            });
            if (companyFromStartingQuery.userId !== req.userId) {
              next({
                message: "You are not the creator of this company",
                errCode: 403,
              });
            } else {
              database.query(
                "update Companies set name=?, website=?, foundedDate=?, numberOfEmployees=? where id=?",
                [name, website, foundedDate, numberOfEmployees, companyId],
                (err, result) => {
                  if (err) {
                    logger.log("error updating company", companyId);
                    next({
                      message: err.message,
                      errCode: 500,
                    });
                  } else if (result.affectedRows === 0) {
                    logger.log("company does not exist");
                    next({
                      message: "company does not exist",
                      errCode: 404,
                    });
                  } else if (result) {
                    let company = {};
                    company.userId = userId;
                    company.name = name;
                    company.website = website;
                    company.foundedDate = foundedDate;
                    company.numberOfEmployees = numberOfEmployees;
                    res.status(200).json(company);
                  }
                }
              );
            }
          }
        }
      }
    );
  },

  delete: (req, res, next) => {
    logger.log("company_controller.delete called");

    const companyId = req.params.companyId;

    let companyFromStartingQuery = {};
    database.query(
      "select * from Companies where id = ?",
      companyId,
      (err, result) => {
        logger.log(result);
        if (result === 0) {
          next({
            message: "company not found",
            errCode: 404,
          });
        } else {
          Object.keys(result).forEach(function (key) {
            companyFromStartingQuery = result[key];
          });
          if (companyFromStartingQuery.userId !== req.userId) {
            next({
              message: "You are not the creator of this company",
              errCode: 401,
            });
          } else {
            database.query(
              "delete from Companies where id = ?",
              companyId,
              (err, result) => {
                if (err) {
                  logger.log("error deleting company", companyId);
                  next({
                    message: err.message,
                    errCode: 500,
                  });
                } else if (result.affectedRows === 0) {
                  logger.log("company does not exist");
                  next({
                    message: "company does not exist",
                    errCode: 404,
                  });
                } else {
                  res.status(200).json(companyFromStartingQuery);
                }
              }
            );
          }
        }
      }
    );
  },
};
