const express = require("express");
const userController = require("../controllers/user_controller");
const authController = require("../controllers/authentication_controller");
const router = express.Router();
const logger = require("tracer").console();

// middleware that is specific to this router
router.use(function (req, res, next) {
  logger.log("Time: ", Date.now().toLocaleString());
  next();
});

router.post("/user", authController.validateToken, userController.create);

router.get("/user", userController.getAll);
router.get("/user/:userId", userController.getById);

router.put(
  "/user/:userId",
  authController.validateToken,
  userController.update
);

router.delete(
  "/user/:userId",
  authController.validateToken,
  userController.delete
);

module.exports = router;
