const express = require("express");
const companyController = require("../controllers/company_controller");
const authController = require("../controllers/authentication_controller");
const router = express.Router();
const logger = require("tracer").console();

// middleware that is specific to this router
router.use(function (req, res, next) {
  logger.log("Time: ", Date.now().toLocaleString());
  next();
});

router.post("/company", authController.validateToken, companyController.create);

router.get("/company", companyController.getAll);
router.get("/company/:companyId", companyController.getById);

router.put(
  "/company/:companyId",
  authController.validateToken,
  companyController.update
);

router.delete(
  "/company/:companyId",
  authController.validateToken,
  companyController.delete
);

module.exports = router;
