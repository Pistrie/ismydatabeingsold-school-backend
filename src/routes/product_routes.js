const express = require("express");
const productController = require("../controllers/product_controller");
const authController = require("../controllers/authentication_controller");
const router = express.Router();
const logger = require("tracer").console();

// middleware that is specific to this router
router.use(function (req, res, next) {
  logger.log("Time: ", Date.now().toLocaleString());
  next();
});

router.post("/product", authController.validateToken, productController.create);

router.get("/product", productController.getAll);
router.get("/product/:productId", productController.getById);

router.put(
  "/product/:productId",
  authController.validateToken,
  productController.update
);

router.delete(
  "/product/:productId",
  authController.validateToken,
  productController.delete
);

module.exports = router;
