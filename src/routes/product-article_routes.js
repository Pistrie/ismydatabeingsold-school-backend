const express = require("express");
const productArticleController = require("../controllers/product-article_controller");
const authController = require("../controllers/authentication_controller");
const router = express.Router();
const logger = require("tracer").console();

// middleware that is specific to this router
router.use(function (req, res, next) {
  logger.log("Time: ", Date.now().toLocaleString());
  next();
});

router.post(
  "/product-article",
  authController.validateToken,
  productArticleController.create
);

router.get("/product-article", productArticleController.getAll);
router.get(
  "/product-article/:productArticleId",
  productArticleController.getById
);

router.put(
  "/product-article/:productArticleId",
  authController.validateToken,
  productArticleController.update
);

router.delete(
  "/product-article/:productArticleId",
  authController.validateToken,
  productArticleController.delete
);

module.exports = router;
