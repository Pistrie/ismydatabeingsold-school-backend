const database = require("../config/database");
const mysql = require("mysql");
const logger = require("tracer").console();

module.exports = {
  create: function (values, callback) {
    logger.log("studentHomeDao.create called");
    const valuesArray = Object.values(values);

    const query = `
        INSERT INTO studenthome(Name, Address, House_Nr, UserID, Postal_Code, Telephone, City)
        VALUES (?, ?, ?, ?, ?, ?, ?)
    `;
    database.query(query, valuesArray, (err, result) => {
      if (err) {
        logger.log("Error adding student home");
        let message;
        if (err.message.match("ER_DUP_ENTRY")) {
          message = "A home already exists on this address";
        } else {
          message = err.message;
        }
        return callback(message, undefined);
      } else if (result) {
        let studenthome = {};
        studenthome.id = result.insertId;
        studenthome.name = values.name;
        studenthome.street = values.street;
        studenthome.number = values.number;
        studenthome.userId = values.userId;
        studenthome.postalCode = values.postalCode;
        studenthome.phoneNumber = values.phoneNumber;
        studenthome.city = values.city;
        // logger.debug(studenthome)
        return callback(undefined, studenthome);
      }
    });
  },
};
