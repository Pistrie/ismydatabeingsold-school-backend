const database = require("../config/database");
const logger = require("tracer").console();

module.exports = {
  create: function (values, callback) {
    logger.log("user_dao.create called");
    const valuesArray = Object.values(values);

    const query = `
        insert into Users(firstName, lastName, emailAddress, dateOfBirth, role, password)
        values (?, ?, ?, ?, ?, ?)
    `;
    database.query(query, valuesArray, (err, result) => {
      if (err) {
        logger.log("error adding user");
        let message;
        if (err.message.match("ER_DUP_ENTRY")) {
          message = "this user already exists";
        } else {
          message = err.message;
        }
        return callback(message, undefined);
      } else if (result) {
        let user = {};
        user.id = result.insertId;
        user.firstName = valuesArray[0];
        user.lastName = valuesArray[1];
        user.emailAddress = valuesArray[2];
        user.dateOfBirth = valuesArray[3];
        user.role = valuesArray[4];
        user.password = valuesArray[5];
        return callback(undefined, user);
      }
    });
  },
};
