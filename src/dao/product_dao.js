const database = require("../config/database");
const logger = require("tracer").console();

module.exports = {
  create: function (values, callback) {
    logger.log("product_dao.create called");
    const valuesArray = Object.values(values);

    const query = `
        insert into Products(userId, companyId, name, website, releaseDate, licenseType)
        values (?, ?, ?, ?, ?, ?)
    `;
    database.query(query, valuesArray, (err, result) => {
      if (err) {
        logger.log("error adding product");
        let message;
        if (err.message.match("ER_DUP_ENTRY")) {
          message = "this product already exists";
        } else {
          message = err.message;
        }
        return callback(message, undefined);
      } else if (result) {
        let product = {};
        product.id = result.insertId;
        product.userId = valuesArray[0];
        product.companyId = valuesArray[1];
        product.name = valuesArray[2];
        product.website = valuesArray[3];
        product.releaseDate = valuesArray[4];
        product.licenseType = valuesArray[5];
        return callback(undefined, product);
      }
    });
  },
};
