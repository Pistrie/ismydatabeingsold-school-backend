const database = require("../config/database");
const logger = require("tracer").console();

module.exports = {
  create: function (values, callback) {
    logger.log("productArticle_dao.create called");
    const valuesArray = Object.values(values);

    const query = `
        insert into ProductArticles(userId, productId, dateOfWriting, articleType, description)
        values (?, ?, ?, ?, ?)
    `;
    database.query(query, valuesArray, (err, result) => {
      if (err) {
        logger.log("error adding productArticle");
        let message;
        if (err.message.match("ER_DUP_ENTRY")) {
          message = "this productArticle already exists";
        } else {
          message = err.message;
        }
        return callback(message, undefined);
      } else if (result) {
        logger.log(result);
        let productArticle = {};
        productArticle.id = result.insertId;
        productArticle.userId = valuesArray[1];
        productArticle.productId = valuesArray[2];
        productArticle.dateOfWriting = valuesArray[3];
        productArticle.articleType = valuesArray[4];
        productArticle.description = valuesArray[5];
        return callback(undefined, productArticle);
      }
    });
  },
};
