const database = require("../config/database");
const mysql = require("mysql");
const logger = require("tracer").console();

module.exports = {
  create: function (values, callback) {
    logger.log("company_dao.create called");
    const valuesArray = Object.values(values);

    const query = `
        insert into Companies(userId, name, website, foundedDate, numberOfEmployees)
        values (?, ?, ?, ?, ?)
    `;
    database.query(query, valuesArray, (err, result) => {
      if (err) {
        logger.log("error adding company");
        let message;
        if (err.message.match("ER_DUP_ENTRY")) {
          message = "this company already exists";
        } else {
          message = err.message;
        }
        return callback(message, undefined);
      } else if (result) {
        let company = {};
        company.id = result.insertId;
        company.userId = valuesArray[0];
        company.name = valuesArray[1];
        company.website = valuesArray[2];
        company.foundedDate = valuesArray[3];
        company.numberOfEmployees = valuesArray[4];
        return callback(undefined, company);
      }
    });
  },
};
