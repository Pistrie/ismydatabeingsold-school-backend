drop database if exists `ismydatabeingsold-school`;
create database if not exists `ismydatabeingsold-school`;
use `ismydatabeingsold-school`;

drop user if exists 'imdbs_user'@'%';
drop user if exists 'imdbs_user'@'localhost';
create user if not exists 'imdbs_user'@'%' identified by 'secret';
create user if not exists 'imdbs_user'@'localhost' identified by 'secret';

grant select, insert, delete, update on `ismydatabeingsold-school`.* to 'imdbs_user'@'%';
grant select, insert, delete, update on `ismydatabeingsold-school`.* to 'imdbs_user'@'localhost';

drop table if exists `ProductArticles`;
drop table if exists `Products`;
drop table if exists `Companies`;
drop table if exists `Users`;

-- ----------------
-- No dates in future
-- ----------------
-- TODO create trigger to prevent dates like birthdate to be in future

-- ----------------
-- Table `Users`
-- ----------------
drop table if exists `Users`;
create table if not exists `Users`
(
    `id`           int unsigned    not null auto_increment,
    `firstName`    varchar(32)     not null,
    `lastName`     varchar(32)     not null,
    `emailAddress` varchar(70)     not null,
    `dateOfBirth`  date            not null,
    `role`         varchar(20)     not null,
    `password`     char(64) binary not null,
    primary key (`id`),
    constraint UniqueEmailAddress unique (`emailAddress`)
)
    engine = InnoDB;

-- filler
insert into `Users` (`firstName`, `lastName`, `emailAddress`, `dateOfBirth`, `role`, `password`)
values ('Sylvester', 'Roos', 'sylvester.roos@xs4all.nl', '2000-11-23', 'admin', 'password123'),
       ('Dinand', 'Roos', 'dinand.roos@xs4all.nl', '2004-02-13', 'user', 'password123'),
       ('Rik', 'Roos', 'rik.roos@xs4all.nl', '1967-09-17', 'user', 'password123'),
       ('Brigitte', 'Roos', 'brigitte.roos@xs4all.nl', '1974-06-30', 'user', 'password123'),
       ('Jan', 'Janssen', 'jan.janssen@xs4ll.nl', '1990-03-14', 'user', 'password123');

-- ----------------
-- Table `Companies`
-- ----------------
drop table if exists `Companies`;
create table if not exists `Companies`
(
    `id`                int unsigned not null auto_increment,
    `userId`            int unsigned not null,
    `name`              varchar(32)  not null,
    `website`           varchar(70)  not null,
    `foundedDate`       date         not null,
    `numberOfEmployees` int          not null,
    primary key (`id`),
    constraint UniqueName unique (`name`),
    constraint UniqueWebsite unique (`website`)
)
    engine = InnoDB;

alter table `Companies`
    add constraint `fk_Companies_Users_userId`
        foreign key (`userId`) references `Users` (`id`)
            on delete cascade
            on update cascade;

-- filler
insert into `Companies` (`userId`, `name`, `website`, `foundedDate`, `numberOfEmployees`)
values ('1', 'Meta Platforms', 'about.facebook.com', '2004-02-01', '60654'),
       ('2', 'Apple Inc.', 'apple.com', '1976-04-01', '147000'),
       ('3', 'Amazon', 'amazon.com', '1994-07-05', '1468000'),
       ('4', 'Netflix', 'netflix.com', '1997-08-29', '12135'),
       ('5', 'Google LLC', 'google.com', '1998-09-04', '139995');

-- ----------------
-- Table `Products`
-- ----------------
drop table if exists `Products`;
create table if not exists `Products`
(
    `id`          int unsigned not null auto_increment,
    `userId`      int unsigned not null,
    `companyId`   int unsigned not null,
    `name`        varchar(32)  not null,
    `website`     varchar(70)  not null,
    `releaseDate` date         not null,
    `licenseType` varchar(20)  not null,
    primary key (`id`),
    constraint UniqueName unique (`name`),
    constraint UniqueWebsite unique (`website`)
)
    engine = InnoDB;

alter table `Products`
    add constraint `fk_Products_Users_userId`
        foreign key (`userId`) references `Users` (`id`)
            on delete cascade
            on update cascade;

alter table `Products`
    add constraint `fk_Products_Companies_companyId`
        foreign key (`companyId`) references `Companies` (`id`)
            on delete cascade
            on update cascade;

-- filler
insert into `Products` (`userId`, `companyId`, `name`, `website`, `releaseDate`, `licenseType`)
values ('1', '1', 'Facebook', 'facebook.com', '2004-06-29', 'copyright'),
       ('2', '2', 'iOS', 'apple.com/ios', '2007-06-29', 'copyright'),
       ('3', '3', 'Echo', 'alexa.amazon.com', '2014-11-06', 'copyright'),
       ('4', '4', 'Netflix', 'netflix.com', '1998-04-14', 'copyright'),
       ('5', '5', 'Google Search', 'google.com', '1997-01-01', 'copyright');

-- ----------------
-- Table `ProductArticles`
-- ----------------
drop table if exists `ProductArticles`;
create table if not exists `ProductArticles`
(
    `id`            int unsigned   not null auto_increment,
    `userId`        int unsigned   not null,
    `productId`     int unsigned   not null,
    `dateOfWriting` date           not null,
    `articleType`   varchar(40)    not null,
    `description`   varchar(10000) not null,
    primary key (`id`)
)
    engine = InnoDB;

alter table `ProductArticles`
    add constraint `fk_ProductArticles_Users_userId`
        foreign key (`userId`) references `Users` (`id`)
            on delete cascade
            on update cascade;

alter table `ProductArticles`
    add constraint `fk_ProductArticles_Products_productId`
        foreign key (`productId`) references `Products` (`id`)
            on delete cascade
            on update cascade;

-- filler
insert into `ProductArticles` (`userId`, `productId`, `dateOfWriting`, `articleType`, `description`)
values ('1', '1', '2021-04-12', 'What is collected?',
        'Facebook collects a large amount of personal data like your online behaviour.'),
       ('2', '2', '2021-02-12', 'How is the data used?',
        'One of the ways Apple uses the data is to improve it\'s services.'),
       ('3', '3', '2021-02-12', 'How is the data used?',
        'The data collected by Echo is used to create a user profile in order to sell more targeted.'),
       ('4', '4', '2021-02-12', 'How is the data used?',
        'Netflix uses the collected data to tailor the user experience.'),
       ('5', '5', '2021-02-12', 'How is the data used?',
        'Google uses the collected user data to show personalized search results.');
