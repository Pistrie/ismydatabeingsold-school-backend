const express = require("express");
const authRoutes = require("./src/routes/authentication_routes");
const userRoutes = require("./src/routes/user_routes");
const companyRoutes = require("./src/routes/company_routes");
const productRoutes = require("./src/routes/product_routes");
const productArticleRoutes = require("./src/routes/product-article_routes");
const logger = require("tracer").console();

const ip = process.env.IP || "127.0.0.1";
const port = process.env.PORT || 3000;

const app = express();
const cors = require("cors");
app.use(express.json());

app.use(cors());

app.all(
  "*",
  (req, res, next) => {
    logger.log("Generic logging handler called");
    next();
  },
  (req, res, next) => {
    const reqMethod = req.method;
    const reqUrl = req.url;
    logger.log(reqMethod + " request at " + reqUrl);
    next();
  }
);

app.get("/", (req, res) => {
  res.send("Welcome to the app");
});

app.use("/api", authRoutes);
app.use("/api", userRoutes);
app.use("/api", companyRoutes);
app.use("/api", productRoutes);
app.use("/api", productArticleRoutes);

app.all("/secret", (req, res) => {
  let responseHtmlButton =
    "<a href='http://www.hat.net/abs/noclick/' ><img src='http://www.hat.net/abs/gif/noclick.gif' alt='[Do not click here!]' ></a>";
  res.set("Content-type", "text/html");
  res.send(
    "<p>You have found the secret location! Here, take this:</p>\n" +
      responseHtmlButton
  );
});

app.get("/api/info", (req, res) => {
  logger.log("Info endpoint called");
  const info = {
    name: "Sylvester Roos",
    studentNumber: "2159313",
    description: "My Nodejs server",
  };
  res.status(200).json(info);
});

app.all("*", (req, res, next) => {
  logger.log("Catch-all endpoint called");
  next({
    message: "Endpoint " + req.url + " does not exist",
    errCode: 401,
  });
});

app.use((error, req, res, next) => {
  logger.log("Errorhandler called!", error);
  res.status(error.errCode).json({
    error: error.errCode,
    // error: "some error occured",
    message: error.message,
  });
});

app.listen(port, () => {
  logger.log(`Example app listening at http://${ip}:${port}`);
});

module.exports = app;
