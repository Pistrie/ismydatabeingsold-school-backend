process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb"
process.env.NODE_END = "testing"
process.env.LOGLEVEL = "debug"

const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../../server")
const database = require("../../src/config/database")
const logger = require("../../src/config/config").logger
const jwt = require("jsonwebtoken")

chai.should()
chai.use(chaiHttp)

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`)

/**
 * Query om alle tabellen leeg te maken, zodat je iedere testcase met
 * een schone lei begint. Let op, ivm de foreign keys is de volgorde belangrijk.
 *
 * Let ook op dat je in de dbconfig de optie multipleStatements: true toevoegt!
 */
const CLEAR_USER_TABLE = "DELETE IGNORE FROM `user`;"
const CLEAR_STUDENTHOME_TABLE = "DELETE IGNORE FROM `studenthome`;"
const CLEAR_MEAL_TABLE = "DELETE IGNORE FROM `meal`;"
const CLEAR_SUBMISSION_TABLE = "DELETE IGNORE FROM `participants`"
const CLEAR_DB = CLEAR_USER_TABLE + CLEAR_STUDENTHOME_TABLE + CLEAR_MEAL_TABLE

/**
 * Voeg een user toe aan de database.
 * Deze ID kun je als foreign key gebruiken in de andere queries, bv insert studenthomes.
 */
const INSERT_USER =
    "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
    "(\"first\", \"last\", \"name@server.nl\",\"1234567\", \"secret\");"

/**
 * Query om een home toe te voegen.
 * */
const INSERT_HOME =
    "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES " +
    "('Princenhage', 'Princenhage', 11, ?, '4706RX', '061234567891', 'Breda');"

/**
 * Query om een maaltijd toe te voegen
 */
const INSERT_MEALS =
    "INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudentHomeID`, `MaxParticipants`) VALUES " +
    "('Test', 'Dit is een testmaaltijd', 'test ingredienten', 'geen', NOW(), NOW(), '10', ?, ?, '8')," +
    "('Lasagne', 'Dit is lasagne', 'kaas, ham', 'lactose', NOW(), NOW(), '5', ?, ?, '6');"

/**
 * voeg een aanmelding toe
 */
const addSignup = "INSERT INTO participants(UserID, StudenthomeID, MealID, SignedUpOn) VALUES(?, ?, ?, NOW())"

let insertedHomeId
let insertedUserId
let insertedMealId

describe("Submissions", function () {
    before((done) => {
        database.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                console.log("if (err) 1")
                done(err)
            } else {
                database.query(
                    "SELECT * FROM user; SELECT * FROM studenthome",
                    (err, rows, fields) => {
                        if (err) {
                            console.log("if (err) 2")
                            done(err)
                        } else {
                            database.query(INSERT_USER, (err, rows, fields) => {
                                if (err) {
                                    done(err)
                                } else {
                                    insertedUserId = rows.insertId
                                    database.query(INSERT_HOME,
                                        [insertedUserId, insertedUserId],
                                        (err, rows, fields) => {
                                            if (err) {
                                                console.log("if (err) 3")
                                                done(err)
                                            } else {
                                                insertedHomeId = rows.insertId
                                                database.query(INSERT_MEALS,
                                                    [insertedUserId, insertedHomeId, insertedUserId, insertedHomeId],
                                                    (err, rows, fields) => {
                                                        if (err) {
                                                            done(err)
                                                        } else {
                                                            insertedMealId = rows.insertId
                                                            database.query(addSignup, [insertedUserId, insertedHomeId, insertedMealId + 1], (err, result) => {
                                                                if (err) {
                                                                    done(err)
                                                                } else {
                                                                    done()
                                                                }
                                                            })
                                                        }
                                                    })
                                            }
                                        }
                                    )
                                }
                            })
                        }
                    }
                )
            }
        })
    })
    
    describe("UC-401 Sign up for meal", function () {
        it("TC-401-1 should return 401 when not logged in", function(done) {
            chai
                .request(server)
                .post(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}/signup`)
                .set("Authorization", "")
                .send({})
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-401-1 should return 404 when meal does not exist", function(done) {
            chai
                .request(server)
                .post(`/api/studenthome/${insertedHomeId}/meal/999999/signup`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-401-3 should return 200 when succesfully signing up", function(done) {
            chai
                .request(server)
                .post(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}/signup`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    
    describe("UC-402 Sign off for meal", function() {
        it("TC-402-1 should return 401 when not logged in", function(done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}/signoff`)
                .set("Authorization", "")
                .send({})
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-402-2 should return 404 when meal does not exist", function(done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/999999/signoff`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-402-3 should return 404 when submission does not exist", function(done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId-1}/signoff`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-402-4 should return 200 when signoff has been processed successfully", function(done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}/signoff`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    
    describe("UC-403 getting list of submissions", function() {
        it("TC-403-1 should return 401 when not logged in", function(done) {
            chai
                .request(server)
                .get(`/api/meal/${insertedMealId}/participants`)
                .set("Authorization", "")
                .send({})
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-403-2 should return 404 when meal does not exist", function(done) {
            chai
                .request(server)
                .get(`/api/meal/${insertedMealId+9}/participants`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-403-3 should return 200 when list exists", function(done) {
            chai
                .request(server)
                .get(`/api/meal/${insertedMealId + 1}/participants`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    
    describe("UC-404 details about participant", function(done) {
        it("TC-404-1 should return 401 when not logged in", function(done) {
            chai
                .request(server)
                .get(`/api/meal/${insertedMealId + 1}/participants/${insertedUserId}`)
                .set("Authorization", "")
                .send({})
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-404-2 should return 404 when participant does not exist", function(done) {
            chai
                .request(server)
                .get(`/api/meal/${insertedMealId + 1}/participants/${insertedUserId + 9}`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-404-3 should return 200 when correct details have been entered", function(done) {
            chai
                .request(server)
                .get(`/api/meal/${insertedMealId + 1}/participants/${insertedUserId}`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({})
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    
    after((done) => {
        database.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                console.log(`after error: ${err}`)
                done(err)
            } else {
                logger.info("After FINISHED")
                done()
            }
        })
    })
})
