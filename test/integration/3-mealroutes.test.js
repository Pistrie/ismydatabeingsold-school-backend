process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb"
process.env.NODE_END = "testing"
process.env.LOGLEVEL = "debug"

const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../../server")
const database = require("../../src/config/database")
const logger = require("../../src/config/config").logger
const jwt = require("jsonwebtoken")

chai.should()
chai.use(chaiHttp)

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`)

/**
 * Query om alle tabellen leeg te maken, zodat je iedere testcase met
 * een schone lei begint. Let op, ivm de foreign keys is de volgorde belangrijk.
 *
 * Let ook op dat je in de dbconfig de optie multipleStatements: true toevoegt!
 */
const CLEAR_USER_TABLE = "DELETE IGNORE FROM `user`;"
const CLEAR_STUDENTHOME_TABLE = "DELETE IGNORE FROM `studenthome`;"
const CLEAR_MEAL_TABLE = "DELETE IGNORE FROM `meal`;"
const CLEAR_DB = CLEAR_USER_TABLE + CLEAR_STUDENTHOME_TABLE + CLEAR_MEAL_TABLE

/**
 * Voeg een user toe aan de database.
 * Deze ID kun je als foreign key gebruiken in de andere queries, bv insert studenthomes.
 */
const INSERT_USER =
    "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
    "(\"first\", \"last\", \"name@server.nl\",\"1234567\", \"secret\");"

/**
 * Query om een home toe te voegen.
 * */
const INSERT_HOME =
    "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES " +
    "('Princenhage', 'Princenhage', 11, ?, '4706RX', '061234567891', 'Breda');"

/**
 * Query om een maaltijd toe te voegen
 */
const INSERT_MEALS =
    "INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudentHomeID`, `MaxParticipants`)" +
    "VALUES ('Lasagne', 'Dit is lasagne', 'kaas, ham', 'lactose', NOW(), NOW(), '5', ?, ?, '6');"

let insertedHomeId
let insertedUserId
let insertedMealId

describe("Meals", function () {
    before((done) => {
        database.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                console.log("if (err) 1")
                done(err)
            } else {
                database.query(
                    "SELECT * FROM user; SELECT * FROM studenthome",
                    (err, rows, fields) => {
                        if (err) {
                            console.log("if (err) 2")
                            done(err)
                        } else {
                            database.query(INSERT_USER, (err, rows, fields) => {
                                if (err) {
                                    done(err)
                                } else {
                                    insertedUserId = rows.insertId
                                    database.query(INSERT_HOME,
                                        [insertedUserId, insertedUserId],
                                        (err, rows, fields) => {
                                            if (err) {
                                                console.log("if (err) 3")
                                                done(err)
                                            } else {
                                                insertedHomeId = rows.insertId
                                                done()
                                            }
                                        }
                                    )
                                }
                            })
                        }
                    }
                )
            }
        })
    })
    
    describe("UC-301 Create a meal", function () {
        it("TC-301-1 should return 400 is required field is missing", function (done) {
            chai
                .request(server)
                .post(`/api/studenthome/${insertedHomeId}/meal`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .set("content-type", "application/json")
                .send({})
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-301-2 should return 401 if not logged in", function(done) {
            chai
                .request(server)
                .post(`/api/studenthome/${insertedHomeId}/meal`)
                .send({
                    "name": "Lasagne",
                    "description": "Het beste Italiaanse gerecht",
                    "ingredients": "gehakt, pasta, kaas",
                    "allergies": "lactose",
                    "price": "10",
                    "maxParticipants": 6
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-301-3 should return 200 if the meal was succesfully added", function (done) {
            chai
                .request(server)
                .post(`/api/studenthome/${insertedHomeId}/meal`)
                .set("content-type", "application/json")
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({
                    "name": "Lasagne",
                    "description": "Het beste Italiaanse gerecht",
                    "ingredients": "gehakt, pasta, kaas",
                    "allergies": "lactose",
                    "price": "10",
                    "maxParticipants": 6
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    database.query("SELECT ID FROM meal;",
                        (err, rows, fields) => {
                            if (err) {
                                done(err)
                            } else if (rows) {
                                insertedMealId = rows[0].ID
                                logger.debug(`insertedMealId: ${insertedMealId}`)
                                done()
                            }
                        }
                    )
                })
        })
    })
    describe("UC-302 Change a meal", function () {
        // 302-1
        it("TC-302-1 should return 400 if required field is missing", function (done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
                .set("content-type", "application/json")
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({
                    "description": "Het beste Italiaanse gerecht",
                    "ingredients": "Gehakt, pasta, kaas, tomatensaus",
                    "allergies": "Lactose",
                    "price": "15",
                    "maxParticipants": 6
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-302-2 should return 401 when not logged in", function(done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
                .set("content-type", "application/json")
                .set("Authorization", "")
                .send({
                    "name": "Lasagne",
                    "description": "Het beste Italiaanse gerecht",
                    "ingredients": "Gehakt, pasta, kaas, tomatensaus",
                    "allergies": "Lactose",
                    "price": "15",
                    "maxParticipants": 6
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-302-3 should return 401 if user is not owner of data", function(done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
                .set("content-type", "application/json")
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId + 1}, "secret"))
                .send({
                    "name": "Lasagne",
                    "description": "Het beste Italiaanse gerecht",
                    "ingredients": "Gehakt, pasta, kaas, tomatensaus",
                    "allergies": "Lactose",
                    "price": "15",
                    "maxParticipants": 6
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-302-4 should return 404 if the meal does not exist", function (done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId + 9}`)
                .set("content-type", "application/json")
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({
                    "name": "Lasagne",
                    "description": "Het beste Italiaanse gerecht",
                    "ingredients": "Gehakt, pasta, kaas, tomatensaus",
                    "allergies": "Lactose",
                    "price": "15",
                    "maxParticipants": 6
                })
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        // 302-5
        it("TC-302-5 should return 200 if the meal was succesfully changed", function (done) {
            chai
                .request(server)
                .put(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
                .set("content-type", "application/json")
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send({
                    "name": "Lasagne",
                    "description": "Het beste Italiaanse gerecht",
                    "ingredients": "Gehakt, pasta, kaas, tomatensaus",
                    "allergies": "Lactose",
                    "price": "15",
                    "maxParticipants": 6
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("UC-303 Get list with meals", function () {
        it("TC-303-1 should return 200 and a list with 0 or more meals", function (done) {
            chai
                .request(server)
                .get(`/api/studenthome/${insertedHomeId}/meal`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("UC-304 Get details about a meal", function () {
        // 304-1
        it("TC-304-1 should return 404 if the meal does not exist", function (done) {
            chai
                .request(server)
                .get(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId + 9}`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        // 304-2
        it("TC-304-2 should return 200 and a JSON object with the details", function (done) {
            chai
                .request(server)
                .get(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    // UC-305
    describe("UC-305 Delete meal", function () {
        // 305-1
        /**
         * Geen data die als niet meegegeven gezien kan worden?
         */
        // it("TC-305-1 should return 400 if a required field is missing", function (done) {
        //     chai
        //         .request(server)
        //         .delete(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
        //         .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
        //         .set("content-type", "application/json")
        //         .send()
        //         .end((err, res) => {
        //             res.should.have.status(400)
        //             done()
        //         })
        // })
        it("TC-305-2 should return 401 when not logged in", function(done) {
            chai
                .request(server)
                .delete(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
                .set("content-type", "application/json")
                .set("Authorization", "")
                .send()
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-305-3 should return 401 when not the owner of the meal", function(done) {
            chai
                .request(server)
                .delete(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
                .set("content-type", "application/json")
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId + 9}, "secret"))
                .send()
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        // 305-4
        it("TC-305-4 should return 404 if the meal does not exist", function (done) {
            chai
                .request(server)
                .delete(`/api/studenthome/${insertedHomeId}/meal/999999`)
                .set("content-type", "application/json")
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send()
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        // 305-5
        it("TC-305-5 should return 200 if the meal was succesfully deleted", function (done) {
            // logger.debug(`
            // userId: ${insertedUserId}
            // homeId: ${insertedHomeId}
            // mealId: ${insertedMealId}`)
            chai
                .request(server)
                .delete(`/api/studenthome/${insertedHomeId}/meal/${insertedMealId}`)
                .set("content-type", "application/json")
                .set("Authorization", "Bearer " + jwt.sign({id: insertedUserId}, "secret"))
                .send()
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
})
